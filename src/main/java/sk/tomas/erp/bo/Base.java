package sk.tomas.erp.bo;

import lombok.Data;

import java.util.UUID;

@Data
public class Base {

    private UUID uuid;

}
